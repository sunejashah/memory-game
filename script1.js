const gameContainer = document.getElementById("game");

const gif = [
  "gifs/1.gif",
  "gifs/2.gif",
  "gifs/3.gif",
  "gifs/4.gif",
  "gifs/5.gif",
  "gifs/1.gif",
  "gifs/2.gif",
  "gifs/3.gif",
  "gifs/4.gif",
  "gifs/5.gif",
];

// here is a helper function to shuffle an array
// it returns the same array with values shuffled
// it is based on an algorithm called Fisher Yates if you want ot research more
function shuffle(array) {
  let counter = array.length;

  // While there are elements in the array
  while (counter > 0) {
    // Pick a random index
    let index = Math.floor(Math.random() * counter);

    // Decrease counter by 1
    counter--;

    // And swap the last element with it
    let temp = array[counter];
    array[counter] = array[index];
    array[index] = temp;
  }

  return array;
}

let shuffledGif = shuffle(gif);

// this function loops over the array of colors
// it creates a new div and gives it a class with the value of the color
// it also adds an event listener for a click for each card
function createDivsForColors(gif) {
  for (let i = 0; i < gif.length; i++) {
    // create a new div
    const newDiv = document.createElement("div");
    newDiv.style.background = "gifs/1.gif";   

    // give it a class attribute for the value we are looping over
    newDiv.classList.add(gif[i]);
    newDiv.style.backgroundColor ="url(gif[1])"
    
    newDiv.setAttribute("id", i);
   

    // call a function handleCardClick when a div is clicked on
    newDiv.addEventListener("click", handleCardClick);

    // append the div to the element with an id of game
    gameContainer.append(newDiv);
  }
}
let openCards = [];
let count = 0;
let move = 0;
// TODO: Implement this function!
function handleCardClick(event) {
  // you can use event.target to see which element was clicked

  let gif =  event.target.classList.value;
  let img = document.createElement("img");
  img.setAttribute("src",gif);
  event.target.append(img)


  console.log("hello");
}

// when the DOM loads
createDivsForColors(shuffledGif);
